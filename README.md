
Create the virtual environment, activate it, and install the requirements:

>python3 -m venv env
>source env/bin/activate
>python3 -m pip install -r requirements.txt

If you need to install any packages in the environment then you should update the requirements.txt:

>python3 -m pip freeze > requirement.txt 

