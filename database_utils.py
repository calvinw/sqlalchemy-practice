from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine

from models import Base, ProblemList, Problem, Folder

import pytest
import logging
import os

engine = create_engine("sqlite:///problems.db", echo=False)
Session = sessionmaker(bind=engine)

def log_database(msg):
    print(msg) 
    os.system("./print_lists.sh")

def cycle_session(session):
    session.close()
    return Session()

def log_sql_debug(session):
    logging.getLogger('sqlalchemy.engine').setLevel(logging.DEBUG)
    return cycle_session(session)

def log_sql_info(session):
    logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)
    return cycle_session(session)

def log_sql_warning(session):
    logging.getLogger('sqlalchemy.engine').setLevel(logging.WARNING)
    return cycle_session(session)

def delete_data():
    session = Session()
    session.query(Problem).delete()
    session.query(ProblemList).delete()
    session.query(Folder).delete()
    session.commit()

@pytest.fixture(scope='function')
def setup_database():
    Base.metadata.create_all(engine)
    delete_data()
    session = Session()
    yield session
    session.close()

# @pytest.fixture(scope='function')
# def delete_data(setup_database):
#     session = setup_database
#     session.query(Problem).delete()
#     session.query(ProblemList).delete()
#     session.query(Folder).delete()
#     session.commit()
#     yield session

@pytest.fixture(scope='function')
def dataset1(setup_database):
    session = setup_database 
    l = ProblemList(name="List1")
    l.problems.append(Problem(name="Problem1", question="q1", answer="a1"))
    session.add(l)
    session.commit()
    yield session

@pytest.fixture(scope='function')
def dataset2(setup_database):
    session = setup_database 
    l = ProblemList(name="List1")
    l.problems.append(Problem(name="Problem1", question="q1", answer="a1"))
    l.problems.append(Problem(name="Problem2", question="q2", answer="a2"))
    session.add(l)
    session.commit()
    yield session

@pytest.fixture(scope='function')
def dataset3(setup_database):
    session = setup_database 
    l = ProblemList(name="List1")
    l.problems.append(Problem(name="Problem1", question="q1", answer="a1"))
    l.problems.append(Problem(name="Problem2", question="q2", answer="a2"))
    l.problems.append(Problem(name="Problem3", question="q3", answer="a3"))
    session.add(l)
    session.commit()
    yield session

@pytest.fixture(scope='function')
def dataset4(setup_database):
    session = setup_database 
    l = ProblemList(name="List1")
    l.problems.append(Problem(name="Problem1", question="q1", answer="a1"))
    l.problems.append(Problem(name="Problem2", question="q2", answer="a2"))
    session.add(l)
    l = ProblemList(name="List2")
    l.problems.append(Problem(name="Problem4", question="q4", answer="a4"))
    l.problems.append(Problem(name="Problem5", question="q5", answer="a5"))
    session.add(l)
    session.commit()
    yield session

@pytest.fixture(scope='function')
def dataset5(setup_database):
    session = setup_database 
    l1 = ProblemList(name="List1")
    l1.problems.append(Problem(name="Problem1", question="q1", answer="a1"))
    l2 = ProblemList(name="List2")
    l2.problems.append(Problem(name="Problem2", question="q2", answer="a2"))

    folder = Folder(name="Folder1")
    folder.problem_lists.append(l1)
    folder.problem_lists.append(l2)
    session.add(folder)
    session.commit()
    yield session

@pytest.fixture(scope='function')
def dataset6(setup_database):
    session = setup_database 
    folder = Folder(name="Folder1")
    l1 = ProblemList(name="List1")
    l1.problems.append(Problem(name="Problem1", question="q1", answer="a1"))
    folder.problem_lists.append(l1)
    session.add(folder)
    session.commit()
    yield session

