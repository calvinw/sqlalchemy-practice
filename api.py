import models
from schemas import Folder, Problem, ProblemList 
from sqlalchemy.orm import Session
import database as db

from pydantic import validate_arguments

@validate_arguments(config=dict(arbitrary_types_allowed=True))
def get_folder(session:Session, id:int):
    folder =  db.get_folder(session, id)
    return Folder.from_orm(folder)

# @validate_arguments(config=dict(arbitrary_types_allowed=True))
# def get_folders(session:Session):
#     folders =  db.get_folders(session)
#     return folders
