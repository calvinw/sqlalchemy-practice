from models import Base, ProblemList, Problem, Folder
from database_utils import setup_database, dataset1, dataset2, dataset3, dataset4, dataset5, dataset6
from database_utils import log_sql_info, log_sql_debug, log_sql_warning
from database_utils import log_database
import database as db
import pytest

def test_get_folder(dataset6):
    session = dataset6
    folder = db.get(session=session, model=Folder, id=1)
    print(folder)
    assert folder.name == "Folder1" 

def test_get_folders(dataset6):
    session = dataset6
    folders = db.get_folders(session=session)
    assert len(folders) == 1 
    assert folders[0].name == "Folder1" 
    assert folders[0].id == 1 

def test_create_folder(setup_database):
    session = setup_database 
    folder = Folder(name="Folder1")
    folderCreate = db.create_folder(session=session, 
                                    folder=folder)
    folders = db.get_folders(session=session)

    assert len(folders) == 1 
    assert folder.name == folderCreate.name
    assert folders[0].name == folderCreate.name
    assert folders[0].id == folderCreate.id

def test_delete_folder(dataset6):
    session = dataset6 
    db.delete_folder(session=session, folderid=1)
    folders = db.get_folders(session=session)
    assert len(folders) == 0

def test_get_list(dataset1):
    session = dataset1
    l = db.get(session=session, model=ProblemList, id=1)
    assert l.name == "List1" 
    assert len(l.problems) == 1

def test_get_folder_list(dataset6):
    session = dataset6
    l = db.get_folder_list(session=session, folderid=1, index=0)
    assert l.name == "List1" 
    assert len(l.problems) == 1

def test_create_folder_list(dataset6):
    session = dataset6

    returned = db.create_folder_list(session=session, folderid=1, index=0, name="List2")

    lists = db.get_folder_lists(session=session, folderid=1)
    assert len(lists) == 2
    assert lists[0].name == "List2" 
    assert returned.name == "List2"
    assert lists[1].name == "List1" 

def test_delete_folder_list(dataset5):
    session = dataset5

    db.delete_folder_list(session=session, folderid=1, index=0)

    lists = db.get_folder_lists(session=session, folderid=1)
    assert len(lists) == 1 
    assert lists[0].name == "List2"

def test_get_folder_lists(dataset5):
    session = dataset5

    lists = db.get_folder_lists(session=session, folderid=1)

    assert len(lists) == 2
    assert lists[0].name == "List1"
    assert lists[1].name == "List2"

def test_get_problem(dataset2):
    session = dataset2
    problem = db.get(session=session, model=Problem, id=2)
    assert problem.name == "Problem2"

def test_get_list_problem(dataset2):
    session = dataset2
    problem = db.get_list_problem(session=session, listid=1, index=1)
    assert problem.name == "Problem2"

def test_create_list_problem(dataset2):
    session = dataset2

    problem = Problem(name="Problem3", question="q3", answer="a3")
    problemCreate = db.create_list_problem(session=session, 
                                          listid=1, 
                                          index=1, 
                                          problem=problem)

    assert isinstance(problemCreate, Problem) == True

    after = db.get(session=session, model=ProblemList, id=1)
    assert len(after.problems) == 3
    assert after.problems[0].name == "Problem1"
    assert after.problems[1].name == "Problem3"
    assert after.problems[2].name == "Problem2"
    assert after.problems[0].position == 1 
    assert after.problems[1].position == 2 
    assert after.problems[2].position == 3 

def test_delete_list_problem(dataset3):
    session = dataset3

    before = db.get(session=session, model=ProblemList, id=1)
    assert len(before.problems) == 3 
    assert before.problems[0].name == "Problem1"
    assert before.problems[1].name == "Problem2"
    assert before.problems[2].name == "Problem3"
    assert before.problems[0].position == 1 
    assert before.problems[1].position == 2 
    assert before.problems[2].position == 3 

    db.delete_list_problem(session=session, listid = 1, index = 1)

    after = db.get(session=session, model=ProblemList, id=1)
    assert len(after.problems) == 2 
    assert after.problems[0].name == "Problem1"
    assert after.problems[1].name == "Problem3"
    assert after.problems[0].position == 1 
    assert after.problems[1].position == 2 

def test_get_list_problems(dataset2):
    session = dataset2
    problems = db.get_list_problems(session=session, listid=1)
    assert len(problems) == 2
    assert problems[0].name == "Problem1"
    assert problems[1].name == "Problem2"


def test_move_list_problem_up(dataset3):
    session = dataset3

    before = db.get(session=session, model=ProblemList, id=1)
    assert len(before.problems) == 3 
    db.move_list_problem(session=session, listid=1, index=2, direction="up")
    after = db.get(session=session, model=ProblemList, id=1)
    assert len(after.problems) == 3 
    assert after.problems[0].name == "Problem1"
    assert after.problems[1].name == "Problem3"
    assert after.problems[2].name == "Problem2"
    assert after.problems[0].position == 1 
    assert after.problems[1].position == 2 
    assert after.problems[2].position == 3 

def test_move_list_problem_down(dataset3):
    session = dataset3
    before = db.get(session=session, model=ProblemList, id=1)
    assert len(before.problems) == 3 

    db.move_list_problem(session=session, listid=1, index=1, direction="down")
    
    after = db.get(session=session, model=ProblemList, id=1)
    assert len(after.problems) == 3 

    assert after.problems[0].name == "Problem1"
    assert after.problems[1].name == "Problem3"
    assert after.problems[2].name == "Problem2"
    assert after.problems[0].position == 1 
    assert after.problems[1].position == 2 
    assert after.problems[2].position == 3 

def test_move_folder_list_up(dataset5):
    session = dataset5

    before = db.get(session=session, model=Folder, id=1)
    assert len(before.problem_lists) == 2 
    assert before.problem_lists[0].name == "List1"
    assert before.problem_lists[1].name == "List2"
    assert before.problem_lists[0].position == 1 
    assert before.problem_lists[1].position == 2 

    db.move_folder_list(session=session, 
                        folderid=1, 
                        index=1, 
                        direction="up")

    after = db.get(session=session, model=Folder, id=1)
    assert len(after.problem_lists) == 2 
    assert after.problem_lists[0].name == "List2"
    assert after.problem_lists[1].name == "List1"
    assert after.problem_lists[0].position == 1 
    assert after.problem_lists[1].position == 2 

def test_move_folder_list_down(dataset5):
    session = dataset5

    before = db.get(session=session, model=Folder, id=1)
    assert len(before.problem_lists) == 2 
    assert before.problem_lists[0].name == "List1"
    assert before.problem_lists[1].name == "List2"
    assert before.problem_lists[0].position == 1 
    assert before.problem_lists[1].position == 2 

    db.move_folder_list(session=session, 
                        folderid=1, 
                        index=0, 
                        direction="down")

    after = db.get(session=session, model=Folder, id=1)
    assert len(after.problem_lists) == 2 
    assert after.problem_lists[0].name == "List2"
    assert after.problem_lists[1].name == "List1"
    assert after.problem_lists[0].position == 1 
    assert after.problem_lists[1].position == 2 


