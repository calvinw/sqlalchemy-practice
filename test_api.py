from schemas import ProblemList, Problem, Folder
from database_utils import setup_database, dataset1, dataset2, dataset3, dataset4, dataset5, dataset6
from database_utils import log_sql_info, log_sql_debug, log_sql_warning
from database_utils import log_database
import models
import api
import pytest

def test_get_folder(dataset6):
    session = dataset6

    folder = api.get_folder(session=session,id=1)

    print(folder)

    assert folder.name == "Folder1" 
    assert folder.id == 1 
    assert isinstance(folder, Folder) == True

# def test_get_folders(dataset6):
#     session = dataset6
#     folders = api.get_folders(session=session)
#     print(type(folders))
#     print(type(folders[0]))
#     assert len(folders) == 1 
#     assert folders[0].name == "Folder1" 
#     assert folders[0].id == 1 

