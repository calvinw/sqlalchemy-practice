
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.orderinglist import ordering_list
from sqlalchemy.orm import relationship
from sqlalchemy import Column, Integer, Text, String, ForeignKey

#import sqlalchemy
#print(sqlalchemy.__version__)

# import logging
# logging.basicConfig(format="%(message)s")
# logging.getLogger('sqlalchemy.engine').setLevel(logging.DEBUG)

Base = declarative_base()

class Folder(Base):
    __tablename__ = 'folders'
    id = Column(Integer, primary_key=True)
    name = Column(String(250))
    problem_lists = relationship("ProblemList", 
                         cascade="all,delete-orphan", 
                         order_by="ProblemList.position", 
                         collection_class=ordering_list("position", count_from=1))

    def __repr__(self):
        return "<Folder(id='%s',name='%s')>" % (self.id, self.name) 
    # def __repr__(self):
    #     list_ids = [] 
    #     lists_str = "\n\tfolder_id|name|[list_id|name\n"
    #     lists_str += "\t----------------------------\n"
    #
    #     for l in self.problem_lists:
    #         list_ids.append(l.id)
    #         lists_str += "\t"
    #         lists_str += str(l)
    #         lists_str += "\n"
    #
    #     vals = (self.id, 
    #             self.name, 
    #             list_ids, 
    #             lists_str)
    #     return "%s|%s|%s|%s" % vals

class ProblemList(Base):
    __tablename__ = 'problemlists'
    id = Column(Integer, primary_key=True)
    name = Column(String(250))
    folder_id = Column(Integer, ForeignKey("folders.id"))
    position = Column(Integer)
    problems = relationship("Problem", 
                             cascade="all,delete-orphan", 
                             order_by="Problem.position", 
                             collection_class=ordering_list("position", count_from=1))

    # def __repr__(self):
        # problem_ids = [] 
        # problems_str = "\n\tlist_id|pos|id|name|ques|ans\n"
        # problems_str += "\t----------------------------\n"
        #
        # for p in self.problems:
        #     problem_ids.append(p.id)
        #     problems_str += "\t"
        #     problems_str += str(p)
        #     problems_str += "\n"
        #
        # vals = (self.id, 
        #         self.name, 
        #         problem_ids, 
        #         problems_str)
        # return "%s|%s|%s|%s" % vals
        #
class Problem(Base):
    __tablename__ = 'problems'
    id = Column(Integer, primary_key=True)
    name = Column(String(250))
    question = Column(Text)
    answer = Column(Text)
    position = Column(Integer)
    problem_list_id = Column(Integer, ForeignKey("problemlists.id"))
    # def __repr__(self):
    #     vals = (self.problem_list_id, 
    #             self.position, 
    #             self.id, 
    #             self.name, 
    #             self.question, 
    #             self.answer)
    #     return "%s|%s|%s|%s|%s|%s" % vals
