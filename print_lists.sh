sqlite3 problems.db <<END_COMMANDS
.mode column
.headers on
select folder_id, position, id, name from problemlists where folder_id = 1 order by position;
select folder_id, position, id, name from problemlists where folder_id = 2 order by position;
select problem_list_id, position, id, name, question, answer from problems where problem_list_id = 1 order by position;
select problem_list_id, position, id, name, question, answer from problems where problem_list_id = 2 order by position;
select problem_list_id, position, id, name, question, answer from problems where problem_list_id = 3 order by position;
END_COMMANDS
