from typing import List, Optional
from pydantic import BaseModel

class Problem(BaseModel):
    id: Optional[int]
    name: str
    question: str
    answer: str
    list_id: Optional[int]
    position: Optional[int]
    class Config:
        orm_mode = True
   
class ProblemList(BaseModel):
    id: Optional[int]
    name: str
    folder_id: Optional[int]
    position: Optional[int]
    problems: List[Problem] = []
    class Config:
        orm_mode = True

class FolderBase(BaseModel):
    name: str

class FolderCreate(FolderBase):
    pass

class Folder(FolderBase):
    id: int 
    problem_lists: List[ProblemList] = []
    class Config:
        orm_mode = True
