from models import Base, ProblemList, Problem, Folder
from sqlalchemy.orm import Session

import os

def switch_positions(items, i,j):
    items[i].position = j+1 
    items[j].position = i+1 

def move_up(items, item):
    index = item.position-1
    if index > 0:
        switch_positions(items,index,index-1)

def move_down(items, item):
    index = item.position-1
    if index < len(items)-1:
        switch_positions(items,index,index+1)

def get(session, model, id:int):
    return session.query(model).filter_by(id=id).one()

def get_folder(session, id:int):
    return session.query(Folder).filter_by(id=id).one()

def get_folders(session):
    return session.query(Folder).all()

def create_folder(session: Session, folder:Folder)->Folder:
    session.add(folder)
    session.commit()
    return folder

def delete_folder(session, folderid:int):
    folder = get(session, Folder, folderid)
    session.delete(folder)
    session.commit()

def get_folder_list(session, folderid:int, index:int):
    folder = get(session, Folder, folderid)
    return folder.problem_lists[index] 

def create_folder_list(session, folderid, index, name):
    folder = get(session, Folder, folderid)
    l = ProblemList(name=name)
    folder.problem_lists.insert(index, l)
    session.commit()
    return l

def delete_folder_list(session, folderid, index):
    folder = get(session, Folder, folderid)
    l = folder.problem_lists[index]
    folder.problem_lists.remove(l)
    session.commit()
    
def get_folder_lists(session, folderid):
    folder = get(session, Folder, folderid)
    return folder.problem_lists 

def get_list_problem(session, listid, index):
    l = get(session, ProblemList, listid)
    return l.problems[index] 

def create_list_problem(session:Session, 
                        listid:int, 
                        index:int, 
                        problem:Problem)->Problem: 
    l = get(session, ProblemList, listid)
    l.problems.insert(index, problem)
    session.commit()
    return problem

def delete_list_problem(session, listid, index):
    l = get(session, ProblemList, listid)
    problem = l.problems[index]
    l.problems.remove(problem)
    session.commit()

def get_list_problems(session, listid):
    l = get(session, ProblemList, listid)
    return l.problems

def move_list_problem(session, listid, index, direction):

    l = get(session, ProblemList, listid)
    problem = l.problems[index] 

    if direction == "up": 
        move_up(l.problems, problem)

    if direction == "down": 
        move_down(l.problems, problem)

    session.flush()
    session.commit()

def move_folder_list(session, folderid, index, direction):

    folder = get(session, Folder, folderid)
    l = folder.problem_lists[index] 

    if direction == "up": 
        move_up(folder.problem_lists, l)

    if direction == "down": 
        move_down(folder.problem_lists, l)

    session.flush()
    session.commit()

